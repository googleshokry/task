<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/attends', function () {
    $days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    $dates = [];
    $input_days = json_decode(\request()->days);
    asort($input_days);
    $end_dates = (\request()->sessions * 30);
    $length = $end_dates / count($input_days);
    $now = new \Carbon\Carbon(\request()->start_date);
    for ($i = 0; $i < $length; $i++) {
        foreach ($input_days as $day) {
            if ($end_dates > count($dates)) {
                $now = $now->next($days[$day]);
                $dates[] = $now->toDateString();
            }
        }
    }
    return $dates;
});
